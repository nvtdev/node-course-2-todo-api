var mongoose = require('mongoose');

mongoose.Promise = global.Promise; // set Mongoose to use native promises
mongoose.connect(process.env.MONGODB_URI);

module.exports = {mongoose};
