//const MongoClient = require('mongodb').MongoClient;
const {MongoClient, ObjectID} = require('mongodb');

var obj = new ObjectID();
console.log(obj);

MongoClient.connect('mongodb://localhost:27017/TodoApp', (err, db) => {
  if (err) {
    console.log('Unable to connect to MongoDB server.');
  }
  console.log('Connected to MongoDB server');

  var query = {
    //completed: false
    _id: new ObjectID('593bfbedb0a0bc6db08a4e8c')
  };

  // db.collection('Todos').find(query).toArray().then((docs) => {
  //   console.log('Todos');
  //   console.log(JSON.stringify(docs, undefined, 2));
  // }, (err) => {
  //   console.log("Unable to fetch todos", err);
  // });

  // db.collection('Todos').find(query).count().then((count) => {
  //   console.log('Todos count:', count);
  // }, (err) => {
  //   console.log("Unable to fetch todos", err);
  // });

  db.close();
});
